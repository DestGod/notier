export function notier(position, text, color) {
    let block = document.createElement('div'),
        styles = [
            {style: 'backgroundColor', value: color},
            {style: 'padding', value: '.5rem'},
            {style: 'display', value: 'block'},
            {style: 'position', value: 'fixed'},
            {style: 'left', value: position.x + 'px'},
            {style: 'top', value: position.y + 'px'},
            {style: 'opacity', value: '.7'},
        ];

    styles.forEach((style) => {
        block.style[style.style] = style.value;
    });

    block.innerText = text;

    document.body.appendChild(block);
}